package com.anderhurtado.spigot.commandblock.editorplus.commands;

import com.anderhurtado.spigot.commandblock.editorplus.CBEP;
import com.anderhurtado.spigot.commandblock.editorplus.objects.Apply;
import com.anderhurtado.spigot.commandblock.editorplus.objects.Command;
import com.anderhurtado.spigot.commandblock.editorplus.objects.Run;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.CommandBlock;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Settype implements Command{

    public void execute(CommandSender j,String message){
        Type t=Type.search(message);
        if(t==null){
            Type[] available=Type.values();
            String av=available[0].name();
            for(int x=1;x<available.length;x++)av+=", "+available[x].name();
            j.sendMessage(CBEP.MESSAGES.get("command.settype.available").replace("%available%",av));
            return;
        }j.sendMessage(CBEP.MESSAGES.get("command.apply.settype"));
        new Apply((Player)j,new Run(){
            public void run(Block b){
                BlockState bs=b.getState();
                String cmd="";
                if(bs instanceof CommandBlock)cmd=((CommandBlock)bs).getCommand();
                b.setType(t.M);
                CommandBlock cb=((CommandBlock)b.getState());
                cb.setCommand(cmd);
                cb.update();
                j.sendMessage(CBEP.MESSAGES.get("action.settype"));
            }
        });
    }

    enum Type{
        NORMAL(Material.COMMAND),CHAIN(Material.COMMAND_CHAIN),REPEATING(Material.COMMAND_REPEATING);

        Material M;
        Type(Material m){
            M=m;
        }

        static Type search(String s){
            if(s==null)return null;
            for(Type t:values())if(t.name().equalsIgnoreCase(s))return t;
            return null;
        }
    }
}
