package com.anderhurtado.spigot.commandblock.editorplus.commands;

import com.anderhurtado.spigot.commandblock.editorplus.CBEP;
import com.anderhurtado.spigot.commandblock.editorplus.objects.Apply;
import com.anderhurtado.spigot.commandblock.editorplus.objects.Command;
import com.anderhurtado.spigot.commandblock.editorplus.objects.Run;
import org.bukkit.block.Block;
import org.bukkit.block.CommandBlock;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Read implements Command{
	public void execute(CommandSender j,String txt){
		j.sendMessage(CBEP.MESSAGES.get("command.apply.read"));
		new Apply((Player)j,new Run(){
			public void run(Block b){
				j.sendMessage(CBEP.MESSAGES.get("action.read").replace("%cmd%",((CommandBlock)b.getState()).getCommand()));
			}
		});
	}
}
